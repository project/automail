CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers
 * Benefits

INTRODUCTION
------------

The Automail module automatically send email to selected node type
and specific user role. 

This module will send email after cron run.

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

INSTALLATION
------------

 * Install the Automail module as you would normally install a
   contributed Drupal module.
   Visit https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

 1. Navigate to Administration > Extend and enable the module.
 2. Navigate to Administration > Configuration > Auto Mail > Settings
    for configuration.

MAINTAINERS
-----------
Current maintainers:
 * Kuldeep Mehra (KuldeepM) - https://www.drupal.org/u/kuldeepm

BENEFITS
-----------

This will automatically send email. So, user can get notification about
newly created content on site.
