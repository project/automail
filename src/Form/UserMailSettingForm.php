<?php

namespace Drupal\automail\Form;

/**
 * @file
 * Contains Drupal\automail\Form\UserMailSettingForm.
 */

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * UserMailSettingForm controller.
 */
class UserMailSettingForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['automail.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_setting_form';
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $configFactory, EntityTypeManagerInterface $entity_type_manager) {
    $this->configFactory = $configFactory;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $roles = [];
    $options = [];
    $config = $this->configFactory->get('automail.settings');
    $form['user_fieldset'] = [
      '#type' => 'details',
      '#title' => $this->t('User Roles'),
      '#open'  => TRUE,
    ];
    $roles = $this->entityTypeManager->getStorage('user_role')->loadMultiple();
    foreach ($roles as $role => $roleObj) {
      if ($role != 'anonymous' && $role != 'authenticated') {
        $options[$role] = $roleObj->get('label');
      }
    }
    $form['user_fieldset']['user_roles_list'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Please choose user role for email.'),
      '#options' => $options,
      '#default_value' => empty($config->get('user_roles_list')) ? [] : $config->get('user_roles_list'),
      '#description' => $this->t("Select roles that can be assigned to receive a email."),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config('automail.settings')
      ->set('user_roles_list', array_keys(array_filter($form_state->getValue('user_roles_list'))))
      ->save();
    drupal_flush_all_caches();
  }

}
